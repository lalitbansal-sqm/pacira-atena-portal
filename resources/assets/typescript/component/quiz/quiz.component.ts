import {Component} from '@angular/core';
import { Observable } from 'rxjs/observable';
import {QuizService} from '../../service/quiz.service';
import 'rxjs/add/operator/map';

@Component({
	moduleId: module.id,
	selector: 'quiz',
	templateUrl: './quiz.component.html',
	providers: [QuizService]
}) 

export class QuizComponent {

	answerArray: Boolean[] = [];
	count: number = 0;
	quizResultArray: Object;
	quizResultPercentage: number;
	userData:any;

	public quizQuestion:QuizQuestion = [
	    {"questionText": "Why is the sky blue?", "answers": [
	      {"answerText":"blah blah 1", "correct": true},
	      {"answerText":"blah blah 2", "correct": false},
	      {"answerText":"blah blah 3", "correct": false}
	      ]},
	    {"questionText": "Why is the meaning of life?", "answers": [
	      {"answerText":"blah blah 1", "correct": true},
	      {"answerText":"blah blah 2", "correct": false},
	      {"answerText":"blah blah 3", "correct": false}
	      ]},
	    {"questionText": "How many pennies are in $10.00?", "answers": [
	      {"answerText":"1,000.", "correct": true},
	      {"answerText":"10,000.", "correct": false},
	      {"answerText":"A lot", "correct": false}
	      ]},
	    {"questionText": "What is the default program?", "answers": [
	      {"answerText":"Hello World.", "correct": false},
	      {"answerText":"Hello Sunshine.", "correct": false},
	      {"answerText":"Hello my ragtime gal.", "correct": true}
	    ]}
	];
	constructor(private _quizService: QuizService) {
		console.log(this.quizQuestion.length);

	}


	getAnswer(index, value) {
		this.answerArray.splice(index, 1, value);
		console.log(this.answerArray);
	}

	quizResultFn() {
		this.answerArray.forEach((item) => {
			if(item == true) {
				this.count +=1;
			}
		})	
		this.quizResultPercentage = (this.count/this.quizQuestion.length)*100;

		this.userData = JSON.parse(localStorage.getItem('userData'));
		this.quizResultArray = {email: this.userData.userDetails.email, quiz_result: 'Pass', percentage: this.quizResultPercentage};

		console.log(this.quizResultArray);

		 this._quizService.postQuizResult(this.quizResultArray).subscribe(res => {
			return res;
		});

		this.quizResultPercentage = 0;
		this.count = 0;
	}
}

interface QuizQuestion {
	[index: number]: {questionText:string, answers:QuizAnswer }
}

interface QuizAnswer {
	[index: number]: {answerText: string, correct: Boolean}
}

interface OnInit { 
  ngOnInit(): void
}