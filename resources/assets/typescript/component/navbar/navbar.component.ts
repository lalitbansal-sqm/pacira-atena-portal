import {Component} from '@angular/core';
import {Router} from '@angular/router';


@Component({
	moduleId: module.id,
	selector: 'nav-bar',
	templateUrl: './navbar.component.html'
})


export class NavBarComponent {
	userData:Object;
	constructor(private route: Router) {
		this.userData = JSON.parse(localStorage.getItem('userData'));

		console.log(this.userData);
	}

	logout() {
		localStorage.clear();
		this.route.navigate(['']);
	}
}


