import {Component} from '@angular/core';
import {Validators, FormBuilder, FormGroup, FormControl,ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/observable';
import {LoginService} from '../../service/login.service';

@Component({
	moduleId: module.id,
	selector: 'home',
	templateUrl: './home.component.html',
	providers: [LoginService]
})


export class HomeComponent {
	private loginForm: FormGroup;
	public login:Object = {email: '', npi: ''};
	public serverMessageError:any;

	constructor(private formBuilder: FormBuilder, private _loginService:LoginService, private router: Router){
		console.log(this.login);
		this.loginForm = formBuilder.group({
	  		email: [null, Validators.compose([Validators.required, Validators.minLength(4)])],
	  		npi: [null, Validators.compose([Validators.required, Validators.pattern, Validators.minLength(4)])]
	  	})
	}

	signInUser(event:Event): any {
		event.preventDefault()
		console.log(this.loginForm.valid, this.loginForm.controls.npi.errors);

		if(this.loginForm.valid) {
			this._loginService.getToken(this.login).subscribe(res => {
				if(res.status_code == 200) {
					this._loginService.login({token: res.tokenValue.token, status_code: res.status_code}).subscribe(res => {
						localStorage.setItem('userData', JSON.stringify(res));
						console.log(localStorage.getItem('userData'));
						this.router.navigate(['/dashboard']);
					})
				} else {	
					this.serverMessageError = true;
				}
			})

		}
	}
}