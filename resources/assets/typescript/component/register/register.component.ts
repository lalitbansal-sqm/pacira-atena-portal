import {Component} from '@angular/core';
import {Validators, FormBuilder, FormGroup, FormControl,ReactiveFormsModule } from '@angular/forms';
import {RegisterService} from '../../service/register.service';
import { Observable } from 'rxjs/observable';

@Component({
	moduleId: module.id,
	selector: 'register',
	templateUrl: './register.component.html',
	providers: [RegisterService]
})

export class RegisterComponent {
	private registerForm: FormGroup;
	public register:Object = {fName: '', lName: '', title: '', email: '', npi: '', yearOfLic: ''};
	public serverMessageError:any;
	constructor(private formBuilder: FormBuilder, private _registerService: RegisterService) {
		this.registerForm = this.formBuilder.group({
			fName: ['', Validators.required],
			lName: ['', Validators.required],
			title: ['', Validators.required],
			email: ['',  Validators.compose([Validators.required, Validators.pattern])],
			npi: ['', Validators.required],
			yearOfLicence: ['', Validators.required]
		})
	}

	registerUser(event:Event): any {
		event.preventDefault();
		console.log(this.register, this._registerService);
		this._registerService.register(this.register).subscribe(res => {
			if(res.status == 'success') {
				console.log(res);
			} else {
				this.serverMessageError = true
			}
		})

	}
}