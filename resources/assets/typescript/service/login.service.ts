import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/map';


@Injectable()


export class LoginService {

	loginApiUrl: string;
	constructor(private _http:Http) {

	}

	// Get User Token
	getToken(obj):Observable<any> {
		return this._http.post('api/auth/getToken',obj).map(res => {
			console.log(res.json())
			return res.json()
		});
	}

	// User Login
	login(obj) {
		return this._http.post('api/auth/login', obj).map(res => {
			return res.json();
		})
	}
}
