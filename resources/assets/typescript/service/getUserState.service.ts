import {Injectable} from '@angular/core';
import {Http} from '@angular/http';


@Injectable()


export class GetUserState {
	constructor(private _http: Http) {

	}

	userState() {
		this._http.get('').map(res => {
			return res.json();
		})
	}

}