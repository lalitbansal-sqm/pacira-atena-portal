import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/map';


@Injectable()



export class RegisterService {
	registerApiUrl: string;
	constructor(private _http:Http) {

	}
	createAuthorizationHeader(headers:Headers) {
	    headers.append('Authorization', 'Basic ' +
	      btoa('61e6829bf7dd301f8b57f3cf59055437:')); 
	}


	register(obj):Observable<any> {

		var headers = new Headers();
	    this.createAuthorizationHeader(headers);
	    headers.append('Content-Type', 'application/json');
		console.log(obj);
		return this._http.post('api/signup',obj, {
        headers: headers
      	}).map(res => {
			console.log(res.json());
			return res.json()
		});
	}
}