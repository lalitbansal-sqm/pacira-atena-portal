import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent }  from './app.component';
import { NavBarComponent }  from './component/navbar/navbar.component';
import { AuthGuard }  from './service/AuthGuard.service';
import { HomeComponent }  from './component/home/home.component';
import { RegisterComponent }  from './component/register/register.component';
import { DashboardComponent }  from './component/dashboard/dashboard.component';
import { QuizComponent }  from './component/quiz/quiz.component';

console.log(HomeComponent, AppComponent)


const routes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'register', component: RegisterComponent},
	{path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
	{path: 'quiz', component: QuizComponent}
];




@NgModule({
  imports: [ BrowserModule, FormsModule, ReactiveFormsModule, HttpModule, RouterModule.forRoot(routes, { useHash: true, enableTracing: true }) ],
  declarations: [ AppComponent, NavBarComponent, HomeComponent, RegisterComponent, DashboardComponent, QuizComponent],
  providers: [AuthGuard],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
