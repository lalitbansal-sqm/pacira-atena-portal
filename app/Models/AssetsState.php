<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetsState extends Model
{
    //
    protected $table = 'assets_state';
    public $timestamps = false;
    protected $fillable = [
        'email', 'id', 'state',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
