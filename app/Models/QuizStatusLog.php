<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizStatusLog extends Model
{
    //
    protected $table = 'quiz_status_log';
    public $timestamps = false;
    protected $fillable = [
        'email', 'id', 'quiz_result', 'percentage',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
