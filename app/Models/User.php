<?php namespace App\Models;
//use Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class User extends Model implements Authenticatable
{  
    use AuthenticableTrait;
    protected $table = 'users';
    public $timestamps = false;
    protected $fillable = [
        'email', 'npi', 'title', 'fname', 'lname', 'year_of_licence',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public function assets_state()
    {
        return $this->hasOne('App\Models\AssetsState');
    }

    public function quiz_status_log()
    {
        return $this->hasOne('App\Models\QuizStatusLog');
    }
}
