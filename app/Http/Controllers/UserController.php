<?php namespace App\Http\Controllers;

use Request;
use Response;
use Route;
use Input;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User as UserModel;
use App\Models\QuizStatusLog as QuizStatusLog;
use App\Models\AssetsState as AssetsState;
use JWTAuth;
use JWTAuthException;
use App\Http\Controllers\Auth\HttpRequestAuthenticator;
use Auth;


class UserController extends Controller
{   
	private $user;
	protected $securityCheck = null;
    public function __construct(UserModel $user) {
        $this->user = $user;
    }

    public function getToken(Request $input) {

		$credentials = [
            'email' => $input::get('email'),
            'password' => $input::get('npi'),
        ];

       // print_r($credentials);
        $token = null;
        try {
           if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['message' => 'invalid_email_or_password', 'status_code' => 422], 422);
           }
        } catch (JWTAuthException $e) {
            return response()->json(['message' => 'failed_to_create_token', 'status_code' => 500], 500);
        }
        return response()->json(['tokenValue' => compact('token'), 'message' => 'success', 'status_code' => 200]);
	}

	public function signup() {
        
        //http request authentication
		$this->securityCheck = new HttpRequestAuthenticator();

		if (!$this->securityCheck->authenticateRequest()) {
			return Response::json([
					'errorOrigin' => Request::fullUrl(),
					'error' => 'app key is not valid',
					'debug' => "invalid key credentials"
				], 401);
		}

		$email = "";
		$npi = "";
		$lName = "";
		$title = "";
		$yearOfLicence = "";
		$fName = "";
		$userDetails = Request::all();
		$userExists = array();

		if ($userDetails['email']) {
			$userExists = UserModel::where('email', $userDetails['email'])->get();
		}

		$countUser = count($userExists);

		//if user does not exists then create a new user.

        if ($countUser == 0) {

        	$user = new UserModel;
            $user->email = $userDetails['email'];
            $user->password = bcrypt($userDetails['npi']);
            $user->fname = $userDetails['fName'];
            $user->lname = $userDetails['lName'];
            $user->title = $userDetails['title'];
            $user->year_of_licence = $userDetails['yearOfLicence'];
            $user->save();

			return Response::json(['status' => 'success']);
        }

        else {
        	return Response::json(['status' => 'fail']);
        }
	}

	public function getAuthUser(Request $request) {
		$tokenResponse = $request::get('token');
		$status_code = $request::get('status_code');
		if ($status_code == 200) {
			$user = JWTAuth::toUser($tokenResponse);
			$user = $user->getAttributes();
            return response()->json(['status' => 'success', 'userDetails' => array('email' => $user['email'], 'title' => $user['title'], 'fname' => $user['fname'], 'lname' => $user['lname'])]);
		}
        else {
        	return Response::json(['status' => 'fail']);
        }
    }

    public function logQuizStatus(Request $request) {

    	//http request authentication
    	$this->securityCheck = new HttpRequestAuthenticator();

		if (!$this->securityCheck->authenticateRequest()) {
			return Response::json([
					'errorOrigin' => Request::fullUrl(),
					'error' => 'app key is not valid',
					'debug' => "invalid key credentials"
				], 401);
		}

		$quizResults = Request::all();

		$quizStatus = new QuizStatusLog;
        $quizStatus->email = $quizResults['email'];
        $quizStatus->quiz_result = $quizResults['quiz_result'];
        $quizStatus->percentage = $quizResults['percentage'];
        $quizStatus->save();

        return Response::json(['message' => 'The quiz status has been logged']);

    }

}