<?php

namespace App\Http\Controllers\Auth;
use Auth;
use Request;
use App\Http\Controllers\Controller;
use Config;

class HttpRequestAuthenticator extends Controller {

    public function authenticateRequest() 
    {
        if (!empty(Request::header('Authorization'))) { 
            $reqHeader = Request::header('Authorization');
            $keys = explode(' ', $reqHeader);
            $authKeys = base64_decode($keys[1]);
            $authKeys = explode(':',$authKeys);
      
            if ($authKeys[0] == Config::get('services.sqm.appKey')) {
                return true;
            }

            else {

                return false;
            }

        }


        else {
            return false;
        } 

    }

}    