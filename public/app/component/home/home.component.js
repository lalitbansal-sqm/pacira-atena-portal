"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var login_service_1 = require("../../service/login.service");
var HomeComponent = (function () {
    function HomeComponent(formBuilder, _loginService, router) {
        this.formBuilder = formBuilder;
        this._loginService = _loginService;
        this.router = router;
        this.login = { email: '', npi: '' };
        console.log(this.login);
        this.loginForm = formBuilder.group({
            email: [null, forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(4)])],
            npi: [null, forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern, forms_1.Validators.minLength(4)])]
        });
    }
    HomeComponent.prototype.signInUser = function (event) {
        var _this = this;
        event.preventDefault();
        console.log(this.loginForm.valid, this.loginForm.controls.npi.errors);
        if (this.loginForm.valid) {
            this._loginService.getToken(this.login).subscribe(function (res) {
                if (res.status_code == 200) {
                    _this._loginService.login({ token: res.tokenValue.token, status_code: res.status_code }).subscribe(function (res) {
                        localStorage.setItem('userData', JSON.stringify(res));
                        console.log(localStorage.getItem('userData'));
                        _this.router.navigate(['/dashboard']);
                    });
                }
                else {
                    _this.serverMessageError = true;
                }
            });
        }
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'home',
        templateUrl: './home.component.html',
        providers: [login_service_1.LoginService]
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder, login_service_1.LoginService, router_1.Router])
], HomeComponent);
exports.HomeComponent = HomeComponent;

//# sourceMappingURL=home.component.js.map
