"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var quiz_service_1 = require("../../service/quiz.service");
require("rxjs/add/operator/map");
var QuizComponent = (function () {
    function QuizComponent(_quizService) {
        this._quizService = _quizService;
        this.answerArray = [];
        this.count = 0;
        this.quizQuestion = [
            { "questionText": "Why is the sky blue?", "answers": [
                    { "answerText": "blah blah 1", "correct": true },
                    { "answerText": "blah blah 2", "correct": false },
                    { "answerText": "blah blah 3", "correct": false }
                ] },
            { "questionText": "Why is the meaning of life?", "answers": [
                    { "answerText": "blah blah 1", "correct": true },
                    { "answerText": "blah blah 2", "correct": false },
                    { "answerText": "blah blah 3", "correct": false }
                ] },
            { "questionText": "How many pennies are in $10.00?", "answers": [
                    { "answerText": "1,000.", "correct": true },
                    { "answerText": "10,000.", "correct": false },
                    { "answerText": "A lot", "correct": false }
                ] },
            { "questionText": "What is the default program?", "answers": [
                    { "answerText": "Hello World.", "correct": false },
                    { "answerText": "Hello Sunshine.", "correct": false },
                    { "answerText": "Hello my ragtime gal.", "correct": true }
                ] }
        ];
        console.log(this.quizQuestion.length);
    }
    QuizComponent.prototype.getAnswer = function (index, value) {
        this.answerArray.splice(index, 1, value);
        console.log(this.answerArray);
    };
    QuizComponent.prototype.quizResultFn = function () {
        var _this = this;
        this.answerArray.forEach(function (item) {
            if (item == true) {
                _this.count += 1;
            }
        });
        this.quizResultPercentage = (this.count / this.quizQuestion.length) * 100;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.quizResultArray = { email: this.userData.userDetails.email, quiz_result: 'Pass', percentage: this.quizResultPercentage };
        console.log(this.quizResultArray);
        this._quizService.postQuizResult(this.quizResultArray).subscribe(function (res) {
            return res;
        });
        this.quizResultPercentage = 0;
        this.count = 0;
    };
    return QuizComponent;
}());
QuizComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'quiz',
        templateUrl: './quiz.component.html',
        providers: [quiz_service_1.QuizService]
    }),
    __metadata("design:paramtypes", [quiz_service_1.QuizService])
], QuizComponent);
exports.QuizComponent = QuizComponent;

//# sourceMappingURL=quiz.component.js.map
