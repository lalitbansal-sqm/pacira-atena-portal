"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var app_component_1 = require("./app.component");
var navbar_component_1 = require("./component/navbar/navbar.component");
var AuthGuard_service_1 = require("./service/AuthGuard.service");
var home_component_1 = require("./component/home/home.component");
var register_component_1 = require("./component/register/register.component");
var dashboard_component_1 = require("./component/dashboard/dashboard.component");
var quiz_component_1 = require("./component/quiz/quiz.component");
console.log(home_component_1.HomeComponent, app_component_1.AppComponent);
var routes = [
    { path: '', component: home_component_1.HomeComponent },
    { path: 'register', component: register_component_1.RegisterComponent },
    { path: 'dashboard', component: dashboard_component_1.DashboardComponent, canActivate: [AuthGuard_service_1.AuthGuard] },
    { path: 'quiz', component: quiz_component_1.QuizComponent }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, forms_1.ReactiveFormsModule, http_1.HttpModule, router_1.RouterModule.forRoot(routes, { useHash: true, enableTracing: true })],
        declarations: [app_component_1.AppComponent, navbar_component_1.NavBarComponent, home_component_1.HomeComponent, register_component_1.RegisterComponent, dashboard_component_1.DashboardComponent, quiz_component_1.QuizComponent],
        providers: [AuthGuard_service_1.AuthGuard],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;

//# sourceMappingURL=app.module.js.map
