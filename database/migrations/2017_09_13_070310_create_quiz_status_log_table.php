<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizStatusLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('quiz_status_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('quiz_result');
            $table->timestamps();
        });

        Schema::table('quiz_status_log', function (Blueprint $table) {
           $table->foreign('email')->references('email')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('assets_state');
    }
}
